# Route53Updater
A small python script to detect IP address changes and update Route53 records.

# Usage
You need to specify the following environment variables:

`AWS_ACCESS_KEY_ID` `AWS_SECRET_ACCESS_KEY`

Generate both of these from your AWS IAM

`HOSTED_ZONE_ID`

The ID of your Route 53 Hosted Zone

`DOMAIN_NAME`

Domain name you want to update (If you have multiple domain names to update, i.e. subdomain, use comma as separator)

`UPDATE_PERIOD`

How frequent you want to check for IP address changes (seconds)

`TTL`

How long the DNS resolver should keep the record. [Read](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/resource-record-sets-values-basic.html)

`IP_TYPES`

Supports `v4` and `v6`. You can also specify multiple values with comma as seperator.

-------------------------------------
Then run `python updater.py`

# Docker Compose
Enable IPv6 support in Docker [here](https://docs.docker.com/config/daemon/ipv6/).
```
version: "3.8"
services:
  route53:
    image: registry.gitlab.com/kingkingyyk/route53updater:latest
    container_name: route_53_updater
    environment:
        - AWS_ACCESS_KEY_ID=<value>
        - AWS_SECRET_ACCESS_KEY=<value>
        - HOSTED_ZONE_ID=<value>
        - DOMAIN_NAME=<value>
        - UPDATE_PERIOD=30
        - TTL=300
        - IP_TYPES=v4,v6
    network_mode: host
    restart: always
```
# Available Architectures
`linux/arm/v7` `linux/amd64`