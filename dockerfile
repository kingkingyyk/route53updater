FROM python:3.9-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ADD updater.py /
ADD requirements.txt /

RUN python -m pip install -r requirements.txt &&\
    rm requirements.txt

CMD [ "python",  "updater.py" ]