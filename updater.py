import boto3
import requests
import os
import time
import re
import traceback
import logging
import json
import sys
from datetime import datetime

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

#================================ FUNCTIONS ==============================
def update_record(zone_id, domain_name, ipv4_add, ipv6_add):
    client = boto3.client('route53')
    changes = []

    if ipv4_add:
        ipv4_change = {
            'Action': 'UPSERT',
            'ResourceRecordSet': {
                'Name': domain_name,
                'ResourceRecords': [
                    {
                        'Value': ipv4_add
                    }
                ],
                'TTL': 300,
                'Type': 'A',
            },
        }
        changes.append(ipv4_change)

    if ipv6_add:
        ipv6_change = {
            'Action': 'UPSERT',
            'ResourceRecordSet': {
                'Name': domain_name,
                'ResourceRecords': [
                    {
                        'Value': ipv6_add
                    }
                ],
                'TTL': 300,
                'Type': 'AAAA',
            },
        }
        changes.append(ipv6_change)

    if changes:
        logging.info('Starting to update record.')
        response = client.change_resource_record_sets(
            ChangeBatch={
                'Changes': changes,
                'Comment': 'Auto updated by registry.gitlab.com/kingkingyyk/route53updater at '+str(datetime.now())
            },
            HostedZoneId=zone_id
        )
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            logging.info('Record updated successfully!')
        else:
            logging.error('Record update failed!')
            logging.error(response)

def query(query_url, regex):
    try:
        value = requests.get(query_url, timeout=10.0).text.strip()
        if re.match(regex, value):
            return value
    except:
        logging.error(traceback.format_exc())
    return None

def get_ipv4_address(query_url):
    return query(query_url, r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$')

def get_ipv6_address(query_url):
    return query(query_url, r'^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$')

#================================ END ==============================

PUBLIC_IPV4_QUERY_URL = 'http://checkip.amazonaws.com/'
PUBLIC_IPV6_QUERY_URL = 'http://ipv6.icanhazip.com/'
SUPPORTED_IP_TYPES = ['v4', 'v6']

ACCESS_KEY_KEY = 'AWS_ACCESS_KEY_ID'
SECRET_KEY_KEY = 'AWS_SECRET_ACCESS_KEY'
UPDATE_PERIOD_KEY = 'UPDATE_PERIOD'
HOSTED_ZONE_ID_KEY = 'HOSTED_ZONE_ID'
DOMAIN_NAME_KEY = 'DOMAIN_NAME'
TTL_KEY = 'TTL'
IP_TYPES_KEY = 'IP_TYPES'
REQUIRED_ENV_VAR = [ACCESS_KEY_KEY, SECRET_KEY_KEY, UPDATE_PERIOD_KEY, HOSTED_ZONE_ID_KEY, DOMAIN_NAME_KEY, TTL_KEY, IP_TYPES_KEY]
missing = [x for x in REQUIRED_ENV_VAR if x not in os.environ.keys()]
if missing:
    logging.error('The following required environment variables are missing -> ({})'.format(', '.format(missing)))
    sys.exit(1)

ACCESS_KEY = os.environ[ACCESS_KEY_KEY]
SECRET_KEY = os.environ[SECRET_KEY_KEY]
HOSTED_ZONE_ID = '/hostedzone/' + os.environ[HOSTED_ZONE_ID_KEY]
DOMAIN_NAME = os.environ[DOMAIN_NAME_KEY].split(',')
PUBLIC_IP_CHECK_PERIOD = int(os.environ[UPDATE_PERIOD_KEY])
TTL = int(os.environ[TTL_KEY])
IP_TYPES = [x.strip() for x in os.environ[IP_TYPES_KEY].split(',') if x in SUPPORTED_IP_TYPES]
if not IP_TYPES:
    logging.error('The supported IP types are {}!'.format(', '.format(SUPPORTED_IP_TYPES)))
    sys.exit(1)

last_public_ipv4 = None
last_public_ipv6 = None
logging.info('Starting periodic check.')
while True:
    try:
        ipv4 = None
        if 'v4' in IP_TYPES:
            ipv4 = get_ipv4_address(PUBLIC_IPV4_QUERY_URL)
            if ipv4:
                if ipv4 == last_public_ipv4:
                    ipv4 = None
                else:
                    logging.info('IPv4 address changed to {}!'.format(ipv4))
                    last_public_ipv4 = ipv4

        ipv6 = None
        if 'v6' in IP_TYPES:
            ipv6 = get_ipv6_address(PUBLIC_IPV6_QUERY_URL)
            if ipv6:
                if ipv6 == last_public_ipv6:
                    ipv6 = None
                else:
                    logging.info('IPv6 address changed to {}!'.format(ipv6))
                    last_public_ipv6 = ipv6

        if ipv4 or ipv6:
            for name in DOMAIN_NAME:
                update_record(HOSTED_ZONE_ID, name, ipv4, ipv6)
    except KeyboardInterrupt:
        sys.exit(0)
    except:
        logging.error(traceback.format_exc())
    time.sleep(PUBLIC_IP_CHECK_PERIOD)